const express = require('express');
const fs = require("fs");

const router = express.Router();
const app = express();


fs.readdirSync('components')
   .map(filename => filename.replace('.js', ''))
    .forEach(component => app.get(`/api/${component}`, require(`./components/${component}`)))

 app.use(express.static("build"));

app.listen(3000, () => console.log('Listening on :8000'));
