module.exports = (req, res) => {
    res.send([
        {id: 1, title: 'Janitor'},
        {id: 2, title: 'Drone'},
        {id: 3, title: 'Chef'},
    ]);
};
