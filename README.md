# ACME Ltd. Company Website: Remastered

Last year we allocated 16 bucks for a dev to build our glorious company website, which he did in just under an hour - the guy was a genius and people loved it - we've like 3 pageviews a month (coming from our office) now! We've been trying to extend the website's functionality but he keeps insisting the code is unmaintainable and we haven't actually paid him yet... Uhmm... Aaanyways, we've got these 16 bucks here, and given how much better of a dev you are (that guy sucked), you'll definitely be able to do it, right?

Our fate rests upon you brave coder, as you are the only person in the world able to fulfill this mission (because everybody else declined)!

## Requirements

The website should consist of "About", "Team", and "Open positions" sections all presented on separate pages.

### Redesign

All sections should now have the same color scheme. The added global header and open positions list don't have any additional visual states like `hover`, `active`, `selected`, etc., unless otherwise stated in the following requirements sections.

A responsive mobile-friendly design should be implemented, which can be found [here](mobile.png), that activates on mobile phones, tablets, and devices under 800px in width. The global header in the mobile design should be sticky and always visible on the top of the page.

![ACME Ltd. Website Design](redesign.png)

### Navigation

One of the key changes from the previous website version will be the navigation. Instead of having everything on one page, the sections need to be broken down into different pages. These pages can be accessed and navigated to using their URL, while still preserving the SPA nature of the whole website.

To offer a better navigation experience the "About" and "Open positions" sections should be displayed as "ACME Ltd." and "Careers" respectively. The former should use the red accent color, formerly used by the "About" section's heading.

Additionally a global header will be required to provide easy way to navigate between these pages. The header should link to the "About", "Team" and "Open positions" sections.

### About section

Most of the section remains visually unchanged, except for unifying the color and size of the heading with the rest of the website, and changing its contents to "About us".

The section should be served under the `/` (root) URI.

### Team section

The only change in this section is the unification of the color scheme with the rest of the website.

The section should be served under the `/team` URI.

### Open positions section

While the initial state of the section will remain the same, now there's the added ability to click on any job position to get extra information about it. This information should be displayed in a panel right next to the job positions list. Additionally, the selected position should be highlighted by changing its background, so that the user gets visual feedback for their actions.

Clicking on any other job position changes the active selection to it and displays its corresponding information in the side panel.

Clicking on an activated job position closes the information panel and removes its selection highlight.

Information about a job position should be fetched dynamically from the API when the position has been clicked on.

The section should be served under the `/careers` URI.


## Technical requirements

All changes should honor and preserve the current monolithic approach to serving the API and front-end from a single node instance.

The codebase should be moved to and managed under the `git` versioning system.

### Front-end

Front-end navigation should not trigger browser page loads - i.e. all sections should be dynamically loaded and rendered in the original page (SPA). Additionally, visiting any of the sections directly via their URL should go through the same entry-point, allowing React to do the routing dynamically.

All CSS should be rewritten using LESS and compiled to CSS on project build (`npm run build`).

### Back-end

One of the biggest changes to the project would require moving a big part of the hard-coded data to a database. We recommend using the Sequelize ORM node library, and setting up and configuring a MySQL database. A dump of the database with all table creation and data insertion queries should be provided upon task completion.

In order to have a more streamlined API interface a [Swagger documentation](swagger.yaml) has been provided in accordance to which the API responses should be modified. Moreover, the API specification file should be added to the project's codebase, in which file all subsequent API changes should documented.

### Refactoring

The codebase needs to be cleaned up and structured in a better way that allows for easier expansion in the future.

Also, to reduce bugs and regressions in the functionality, we would like to have integration tests added to the project.

### Miscellaneous

There seems to be an issue with the API when running the development environment (`npm run start`) that needs to get fixed.
