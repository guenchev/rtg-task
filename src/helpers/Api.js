class Api {
    static request(uri) {
        return fetch(`/api/${uri}`).then(response => response.json());
    }

    static getEmployees() {
        return Api.request('employees');
    }

    static getOpenings() {
        return Api.request('openings');
    }
}

export default Api;
